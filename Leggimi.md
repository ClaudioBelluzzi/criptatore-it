# Criptatore Unicode

Un altro modo per criptare i tuoi testi.



## Introduzione

Criptatore UNICODE è un software sviluppato in Python da due ragazzi italiani. Lo scopo del progetto è quello di fornire una alternativa open source a tutti i sistemi di crittografia già esistenti.



## Utilizzo

Per utilizzare il software su linux non bisogna fare altro che scaricare la cartella del progetto e rendere il file 'Encoder.py' eseguibile con il comando

```bash
chmod +x Encoder.py
```

Poi avviare il programma con

```bash
./Encoder.py
```



---



Per decrittare i testi crittati con Encoder.py, prima di tutto rendere eseguibile il file 'Decoder.py' con

```bash
chmod +x Decoder.py
```

Poi eseguirlo con

```bash
./Decoder.py
```



## ToDo

- [ ] Aggiungere la possibilità di passare file come argomento

- [ ] Aggiungere la possibilità di ridirezionare l'output a file

- [x] Sistemare e caricare il decoder

- [ ] Aggiungere gli eseguibili per Windows
