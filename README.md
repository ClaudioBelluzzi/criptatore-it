# Unicode Encoder

Another way to encrypt your texts.



## Introduction

Unicode Encoder is a software developed in Python by two Italian boys. The aim of the project is to provide an open source alternative to all existing encryption systems.




## How to use

To use the software on linux all you have to do is download the project folder and make the file 'Encoder.py' executable with the command:

```bash
chmod +x Encoder.py
```

And then execute the program with:

```bash
./Encoder.py
```



---



To decrypt the encrypted texts with Encoder.py, first of all make the 'Decoder.py' file executable with the command:

```bash
chmod +x Decoder.py
```

And then execute it with:

```bash
./Decoder.py
```



## ToDo

- [ ] Add the possibility to pass files as an argument (only txt)

- [ ] Add the possibility to redirect output to file (only txt)

- [x] Adjust and upload the decoder

- [ ] Upload the executables for Windows
