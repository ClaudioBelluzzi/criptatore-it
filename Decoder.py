#!/bin/python

import hashlib
import getpass

def agg_zeri(numero): # Funzione che aggiunge gli zeri iniziali persi nella conversione a numero
	lunghezza=len(str(numero))
	numero_caratteri=(lunghezza//7)+1
	zeri=(numero_caratteri*7)-lunghezza
	numero_str=("0"*zeri)+(str(numero))
	return numero_str

def decoder(numero_str): # Funzione che converte il numero decimale in testo
	stringa=""
	inizio=0
	fine=7
	lunghezza=len(numero_str)/7
	i=0
	while(i<lunghezza):
		numero=int(numero_str[inizio:fine])
		stringa=stringa+(chr(numero))
		i=i+1
		inizio=inizio+7
		fine=fine+7
	return stringa

testo_crpt=input("Inserisci il testo criptato: ")
lenght=len(testo_crpt)-64
pw_orig=testo_crpt[lenght:]
uguali=False
while(not uguali):
	password=getpass.getpass("Inserisci la password: ")
	pass_a=hashlib.sha256(password.encode())
	pass_app=pass_a.hexdigest()
	pass_up=pass_app.upper()
	uguali=True
	if(pass_up!=pw_orig):
		uguali=False
		print("Password errata! Il testo non verrà decriptato. Ritenta.\n")

pass_hash=hashlib.sha512(password.encode())
pass_esa=pass_hash.hexdigest()
testo_crpt_ridotto=testo_crpt[:lenght]

pass_dec=int(pass_esa, 16)
testo_crpt_ridotto_dec=int(testo_crpt_ridotto, 16)
testo_dec=testo_crpt_ridotto_dec-pass_dec
testo_dec_z=agg_zeri(testo_dec)
testo_decriptato=decoder(testo_dec_z)
print("Il testo decriptato è:", testo_decriptato)
