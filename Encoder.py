#!/bin/python

import hashlib
import getpass

def conversione(carattere):
	unicode=str(ord(carattere))          # Ricava il codice Unicode del carattere
	unicode="0"*(7-len(unicode))+unicode # Aggiunge degli '0' in testa per far arrivare i caratteri a 7
	return unicode                       # Ritorna la stringa

# ==================================== MAIN ====================================

stringa=input("Inserisci il testo da criptare: ") # Input del testo da criptare
password_check=True
errore=True
while(errore): # While necessario a inserire la password con conferma
	if(not password_check):
		print("Conferma password fallita. Ritenta.")

	password_vuota=True
	while(password_vuota):
		password=getpass.getpass("Inserisci la password: ")
		if(password!=""):
			password_vuota=False

	password_repeat=getpass.getpass("Conferma la password: ")

	errore=False
	if(password!=password_repeat):
		errore=True
	password_check=False

i=0
posizione=0
lunghezza=len(stringa)
stringa_decimale=""
while(i<lunghezza): # Converto il testo in una stringa contenente il corrispondente codice decimale
	car=stringa[i]
	car=conversione(car)
	stringa_decimale+=car
	i+=1

pass_sha512=hashlib.sha512(password.encode()).hexdigest()         # Creo l'hash della password con lo sha512...
pass_sha256=hashlib.sha256(password.encode()).hexdigest().upper() # ...e con lo sha256

stringa_dec=int(stringa_decimale)
password_dec=int(pass_sha512, 16)       # Trasformo la password con sha512 da esadecimale a decimale
ris=stringa_dec+password_dec            # Sommo testo e password
crpt=hex(ris).split('x')[-1].upper()    # Ritrasformo il tutto in esadecimale per un output più contenuto
crpt+=pass_sha256                       # Gli aggiungo lo sha256 della password alla fine
print("Testo criptato: ", crpt, sep="") # Stampo il risultato finale
